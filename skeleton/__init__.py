# uncomment the import line below to use shorthand import of the form:
# from skeleton import sample_function

# otherwise, long import with module name is required:
# from skeleton.code import sample_function

from .code import sample_function
