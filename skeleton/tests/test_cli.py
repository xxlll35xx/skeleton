"""
Test suite for command line scripts created with entry_points in setup.py.
"""

from unittest import TestCase
from skeleton.command_line import main

from cStringIO import StringIO
import sys

class Capturing(list):
	"""
	Simple context manager to capture stdout.
	Needed to test command line scripts.
	"""
	def __enter__(self):
		self._stdout = sys.stdout
		sys.stdout = self._stringio = StringIO()
		return self
	def __exit__(self, *args):
		self.extend(self._stringio.getvalue().splitlines())
		del self._stringio# free up some memory
		sys.stdout = self._stdout

class TestConsole(TestCase):
	def test_cli(self):
		with Capturing() as s:
			main()
		self.assertTrue(isinstance(s[0], basestring))
