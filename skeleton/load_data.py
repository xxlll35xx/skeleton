"""
Simple script to load included data files.
Data files must be included in MANIFEST.in.
"""

import json
from os.path import join,dirname

def load_data():
	filename = join(dirname(__file__), 'data', 'data.json')
	with open(filename,'r') as f:
		sample_data = json.loads(f.read())
	return sample_data

