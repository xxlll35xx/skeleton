"""
This script is used by entry_points in setup.py to include
command line scripts in a package.

This method has the advantage of being able to be included
in the test suite.
"""

import skeleton

def main():
	print skeleton.sample_function()
