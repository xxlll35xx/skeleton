#!/bin/bash

# Convert README.md to README.rst
# requires pandoc

pandoc --from=markdown --to=rst --output=README.rst README.md
