from setuptools import setup

setup(
	# package name
	name='skeleton',
	# package version
	version='0.1',
	# package description
	description='A minimal python package structure.',
	# link to package github or website
	url='Github url',
	# package author
	author='John Doe',
	# author contact email
	author_email='john.doe@gmail.com',
	# license
	license='MIT',
	# package to install, i.e., the name of this package
	packages=['skeleton'],
	# list of dependencies if required and if on PyPi
	# optional
	install_requires=[],
	# A boolean (True or False) flag specifying whether the project can be safely installed and run from a zip file.
	zip_safe=False,
	# ensure Nose is installed for testing during installation
	# optional
	test_suite = 'nose.collector',
	tests_require = ['nose'],
	# path to any command line scripts
	# this is the easy way to include scripts, but these scripts aren't easily testable
	# optional
	scripts = ['bin/skeleton-cli'],
	# alternatively, scripts included via entry_points can be included in the test suite
	# optional
	entry_points = {
		'console_scripts':['skeleton-cli2=skeleton.command_line:main']
		},
	# include additional data in package install
	# optional
	include_package_data=True
	)

