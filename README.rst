Skeleton
========

Basic skeleton for a simple Python package.
-------------------------------------------

This repository provides the required file structure for creating a
basic Python package. This includes:

-  an exemplar setup.py file
-  two methods for including command line scripts
-  unit tests
-  external data to be distributed with the package

This structure is adapted from
http://python-packaging.readthedocs.io/en/latest/index.html.

setup.py
--------

setup.py is commented and self-explanatory. Arguments required for
including tests, command line scripts, dependencies, and external data
are listed as *optional*, and should only be included if the package
includes or requires these features.

Command Line Scripts
--------------------

Command line utilities can be included in one of two ways:

1. Using the ``scripts=`` argument in setup.py to explicitly specify the
   location of any command line utilities. In this stucture, scripts are
   kept in the ``bin/`` directory. The choice and naming of the
   directory is arbitrary, as long the path is specified in setup.py.
   This is the easiest way to include scripts, but scripts included in
   this manner are difficult to incorporate into a testing environment

   -  An example is provided in ``bin/skeleton-cli``. No tests are
      included for the command line utility in this package.

2. Using the ``entry_points=`` argument in setup.py, and creating a
   function in the skeleton/ subdirectory that is to be executed as a
   command line script. ``entry_points=`` requires takes a dictionary as
   an argument, an example of which is provided in setup.py.

   -  ``skeleton/command_line.py`` provides an example of command line
      utility included in this manner. Here, the ``main()`` function
      represents the function to be executed, and can be called on the
      command line via the ``skeleton-cli2`` command. The
      ``skeleton-cli2`` alias for the ``main()`` function in
      ``skeleton/command_line.py`` is established explicitly in setup.py
      as part the argument supplied to ``entry_points=``
   -  Tests for this utility are included in ``skeleton/tests``. Because
      this utility prints to stdout, a context wrapper is required to
      capture the output for testing. A simple context wrapper is
      provided in ``skeleton/tests/test_cli.py``.

Tests
-----

Exemplar tests are included in ``skeleton/tests``. Tests can be run by
typing ``nosetests -v`` from the top level directory.

``skeleton/tests/test_cli.py`` tests the ``entry_points=``
implementation of a command line utility. The source for this function
can be found in ``skeleton/command_line.py``. This includes a context
wrapper to approprately capture stdout. Because ``command_line.py`` is
not referenced in the top level ``__init__.py``, the long from import
must be used (i.e., ``from skeleton.command_line import main``).

``skeleton/tests/test_function.py`` tests the funciton defined in
``skeleton/code.py``. Here, ``code.py`` is referenced in the top level
``__init__.py``; the short form import can be used in this case (i.e,
``from skeleton import sample_function``).

There is no reason both tests cannot use the short or long form imports.
Including only ``code.py`` in ``__init__.py`` simply allows
demonstration of both forms.

External Data
-------------

Non-\ ``*.py`` files to be included in the package must be explicitly
included in MANIFEST.in. In this example, this README.rst and a simple
JSON file are included as external data.

The JSON file is placed in ``skeleton/data``, although it may be placed
anywhere in the directory. MANIFEST.in must include the exact location
of external data.

``skeleton/load_data.py`` provides a simple function for loading
``skeleton/data/data/json``. This makes it easy for the end user to load
any data included in the package without having to search for its exact
path.
